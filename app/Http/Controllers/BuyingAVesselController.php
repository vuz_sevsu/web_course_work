<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ship;


class BuyingAVesselController extends Controller
{
  public function index()
  {
    $model = new Ship();
    return view('BuyingVesselView', ['model'=>$model]);
  }
  public function edit_information(Request $request)
  {
    if (!isset($_SESSION))
      session_start();
    $str = file_get_contents("assets/pages_storage/purchases_page.json");
    $info_json = json_decode($str, true);
    $info_json[$_SESSION["locale"]]["brief_information"] = $request->input('mytextarea');
    $str = json_encode($info_json, JSON_UNESCAPED_UNICODE);
    file_put_contents('assets/pages_storage/purchases_page.json', $str);
    return redirect()->back();
  }

  public function add_purchase()
  {
    return view('ShipTemplateView', ['_title'=>'purchases_page.main_title']);
  }
}
