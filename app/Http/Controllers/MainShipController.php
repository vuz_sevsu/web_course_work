<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ship;

class MainShipController extends Controller
{
    public function save_ship(Request $request)
    {
      $model = new Ship;

      $file = $request->file('cover_photo');
      //$path = pathinfo($file);
      //$ext = mb_strtolower($path['extension']);
      $img="";
      /*if (in_array($ext, array('jpeg', 'jpg', 'gif', 'png', 'webp', 'svg', 'tmp'))) {
      	if ($ext == 'svg') {
      		$img = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents($file));
      	} else {
      		$size = getimagesize($file);
      		$img = 'data:' . $size['mime'] . ';base64,' . base64_encode(file_get_contents($file));
      	}
      }*/
      $size = getimagesize($file);
      $img = 'data:' . $size['mime'] . ';base64,' . base64_encode(file_get_contents($file));

      $model->cover_photo = $img;
      $model->main_title = $request->input('main_title');

      $model->short_text = $request->input('short_text');
      $model->main_content = $request->input('main_content');

      $model->length_overall = $request->input('length_overall');
      $model->beam_overall = $request->input('beam_overall');
      $model->displacement = $request->input('displacement');
      $model->draft = $request->input('draft');
      $model->quantity_of_cabins = $request->input('quantity_of_cabins');
      $model->range = $request->input('range');
      $model->fuel_capacity = $request->input('fuel_capacity');
      $model->water_capacity = $request->input('water_capacity');
      $model->cruising_speed = $request->input('cruising_speed');
      $model->sleeping_places = $request->input('sleeping_places');
      $model->status = $request->input('status');
      $model->price = $request->input('price');
      $model->type = $request->input('type');
      $model->save();
      return redirect()->back();
    }

    public function open_ship_for_id($id)
    {
      $model = new Ship();
      $ship_info = $model->getShip($id);
      #print_r($news_info);
      return view('ShipTemplateView', ['ship_info'=>$ship_info]);
    }

    public function edit_ship_for_id($id, Request $request)
    {
      $model = new Ship;
      $news_info = $model->getShip($id);
      $file = $request->file('cover_photo');
      if ($file!=null)
      {
        $path = pathinfo($file);
        $ext = mb_strtolower($path['extension']);
        $img = "";
        if (in_array($ext, array('jpeg', 'jpg', 'gif', 'png', 'webp', 'svg', 'tmp'))) {
          if ($ext == 'svg') {
            $img = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents($file));
          } else {
            $size = getimagesize($file);
            $img = 'data:' . $size['mime'] . ';base64,' . base64_encode(file_get_contents($file));
          }
        }
        $model->cover_photo = $img;
      }
      else
      {
        $model->cover_photo = $news_info['0']['cover_photo'];
      }

      $model->main_title = $request->input('main_title');

      $model->short_text = $request->input('short_text');
      $model->main_content = $request->input('main_content');

      $model->length_overall = $request->input('length_overall');
      $model->beam_overall = $request->input('beam_overall');
      $model->displacement = $request->input('displacement');
      $model->draft = $request->input('draft');
      $model->quantity_of_cabins = $request->input('quantity_of_cabins');
      $model->range = $request->input('range');
      $model->fuel_capacity = $request->input('fuel_capacity');
      $model->water_capacity = $request->input('water_capacity');
      $model->cruising_speed = $request->input('cruising_speed');
      $model->sleeping_places = $request->input('sleeping_places');
      $model->status = $request->input('status');
      $model->price = $request->input('price');
      $model->type = $request->input('type');

      Ship::where('id', $id)->update([
        'cover_photo'=>$model->cover_photo,
        'main_title'=>$model->main_title,
        'short_text'=>$model->short_text,
        'main_content'=>$model->main_content,
        'length_overall'=>$model->length_overall,
        'beam_overall'=>$model->beam_overall,
        'displacement'=>$model->displacement,
        'draft'=>$model->draft,
        'quantity_of_cabins'=>$model->quantity_of_cabins,
        'range'=>$model->range,
        'fuel_capacity'=>$model->fuel_capacity,
        'water_capacity'=>$model->water_capacity,
        'cruising_speed'=>$model->cruising_speed,
        'sleeping_places'=>$model->sleeping_places,
        'status'=>$model->status,
        'price'=>$model->price,
        'type'=>$model->type,
      ]);
      return redirect()->back();
    }
}
