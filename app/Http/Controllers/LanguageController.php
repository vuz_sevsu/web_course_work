<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LanguageController extends Controller
{
    public function changeLocale($locale)
    {
      if (!isset($_SESSION)) session_start();
      $_SESSION["locale"]=$locale;
      App::setlocale($locale);
      #echo("controller ".App::getLocale()."</br>");
      return redirect()->back();
    }
}
