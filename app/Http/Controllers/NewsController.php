<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
      $model = new News();
      return view('NewsView', ['model'=>$model]);
    }
    public function edit_information(Request $request)
    {
      if (!isset($_SESSION))
        session_start();
      $str = file_get_contents("assets/pages_storage/news_page.json");
      $info_json = json_decode($str, true);
      $info_json[$_SESSION["locale"]]["brief_information"] = $request->input('mytextarea');
      $str = json_encode($info_json, JSON_UNESCAPED_UNICODE);
      file_put_contents('assets/pages_storage/news_page.json', $str);
      return redirect()->back();
    }
    public function add_news()
    {
      return view('NewsTemplateView');
    }
    public function save_news(Request $request)
    {
      $model = new News;

      $model->language = $request->input('language');
      $file = $request->file('cover_photo');
      $path = pathinfo($file);
      $ext = mb_strtolower($path['extension']);
      $img = "";
      if (in_array($ext, array('jpeg', 'jpg', 'gif', 'png', 'webp', 'svg', 'tmp'))) {
      	if ($ext == 'svg') {
      		$img = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents($file));
      	} else {
      		$size = getimagesize($file);
      		$img = 'data:' . $size['mime'] . ';base64,' . base64_encode(file_get_contents($file));
      	}
      }
      $model->cover_photo = $img;
      $model->main_title = $request->input('main_title');
      $model->short_text = $request->input('short_text');
      $model->main_content = $request->input('main_content');
      $model->save();
      return redirect('/news');
    }

    public function open_news_for_id($news_id)
    {
      $model = new News();
      $news_info = $model->getNews($news_id);
      #print_r($news_info);
      return view('NewsTemplateView', ['news_info'=>$news_info]);
    }

    public function edit_news_for_id($news_id, Request $request)
    {
      $model = new News;
      $news_info = $model->getNews($news_id);
      $file = $request->file('cover_photo');
      if ($file!=null)
      {
        $path = pathinfo($file);
        $ext = mb_strtolower($path['extension']);
        $img = "";
        if (in_array($ext, array('jpeg', 'jpg', 'gif', 'png', 'webp', 'svg', 'tmp'))) {
          if ($ext == 'svg') {
            $img = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents($file));
          } else {
            $size = getimagesize($file);
            $img = 'data:' . $size['mime'] . ';base64,' . base64_encode(file_get_contents($file));
          }
        }
        $model->cover_photo = $img;
      }
      else
      {
        $model->cover_photo = $news_info['0']['cover_photo'];
      }

      $model->main_title = $request->input('main_title');
      $model->short_text = $request->input('short_text');
      $model->main_content = $request->input('main_content');

      News::where('id', $news_id)->update([
        'cover_photo'=>$model->cover_photo,
        'main_title'=>$model->main_title,
        'short_text'=>$model->short_text,
        'main_content'=>$model->main_content
      ]);
      return redirect()->back();
    }
}
