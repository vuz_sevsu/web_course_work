<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditInfoForms\IndexPageBriefInfo;
use Auth;

class MainPageController extends Controller
{
    public function index()
    {
      return view('IndexView');
    }
    public function edit_brief_information(IndexPageBriefInfo $request)
    {
      if (!isset($_SESSION))
        session_start();
      $str = file_get_contents("assets/pages_storage/index_page.json");
      $info_json = json_decode($str, true);
      $info_json[$_SESSION["locale"]]["brief_information"] = $request->input('mytextarea');
      $str = json_encode($info_json, JSON_UNESCAPED_UNICODE);
      file_put_contents('assets/pages_storage/index_page.json', $str);
      return redirect()->back();
    }
}
