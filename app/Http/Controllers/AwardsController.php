<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AwardsController extends Controller
{
  public function index()
  {
    return view('AwardsView');
  }
  public function edit_information(Request $request)
  {
    if (!isset($_SESSION))
      session_start();
    $str = file_get_contents("assets/pages_storage/awards_page.json");
    $info_json = json_decode($str, true);
    $info_json[$_SESSION["locale"]]["brief_information"] = $request->input('mytextarea');
    $str = json_encode($info_json, JSON_UNESCAPED_UNICODE);
    file_put_contents('assets/pages_storage/awards_page.json', $str);
    return redirect()->back();
  }
}
