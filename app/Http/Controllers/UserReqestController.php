<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ship;
use App\User;
use App\User_request;

class UserReqestController extends Controller
{
    public function add_request($ship_id)
    {
      return view('UserRequestView', ['ship_id'=>$ship_id]);
    }

    public function save_request($id, Request $request)
    {
      $ship = new Ship();
      $ship_info = $ship->getShip($id);
      $model = new User_request();

      $model->user_id = json_decode($_SESSION["current_user"])->id;
      $model->ship_id = $id;
      $model->number_phone = $request->input('number_phone');
      $model->mail = $request->input('mail');
      $model->first_name = $request->input('first_name');
      $model->second_name = $request->input('second_name');
      $model->patronymic = $request->input('patronymic');
      $model->comment = $request->input('comment');
      $model->save();

      Ship::where('id',$id)->update([
        'status'=>"consideration",
      ]);

      return redirect('/');
    }

    public function admin_rent_pending()
    {
      $user_request = new User_request();
      $ship = new Ship();
      return view('AdminRentPendingView', ['model'=>$user_request, 'ship_model'=>$ship]);
    }

    public function admin_purchase_pending()
    {
      $user_request = new User_request();
      return view('AdminPurchasePendingView', ['model'=>$user_request]);
    }

    public function admin_pending($id)
    {
      $user_request = new User_request();
      $request_info = $user_request->getAdminPendingRequests($id);
      return view('UserRequestView', ['request_info'=>$request_info]);
    }

    public function edit_user_request($id, Request $request)
    {
      #var_dump($id);
      $model = new User_request();
      if ($request->input('in_work_button')!=null)
      {
        User_request::where('id',$id)->update([
          'status'=>"in_work",
        ]);
      }
      if ($request->input('done_button')!=null)
      {
        User_request::where('id',$id)->update([
          'status'=>"done",
        ]);
      }
      if ($request->input('blocked_button')!=null)
      {
        User_request::where('id',$id)->update([
          'status'=>"blocked",
        ]);
      }
      return redirect('/');
    }

    public function admin_requests_in_work()
    {
      $user_request = new User_request();
      return view('AdminRequestsInWorkView', ['model'=>$user_request]);
    }
}
