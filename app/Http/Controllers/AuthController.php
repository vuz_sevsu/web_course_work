<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function after_auth()
    {
      if (!isset($_SESSION)) session_start();
      if (Auth::user()->isAdmin == '1')
      {
        $_SESSION["isAdmin"] = true;
      }
      else {
        $_SESSION["isUser"] = true;
        $_SESSION["current_user"] = json_encode(Auth::user());
      }
      return redirect('/');
    }
    public function end_session()
    {
      if (!isset($_SESSION)) session_start();
      if (isset($_SESSION["isUser"]))
      {
          unset($_SESSION["isUser"]);
          unset($_SESSION["current_user"]);
      }
      if (isset($_SESSION["isAdmin"])) unset($_SESSION["isAdmin"]);

      return redirect('/');
    }
}
