<?php

namespace App\Http\Middleware;

use Closure;
use App;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset($_SESSION)) session_start();
        if (isset($_SESSION["locale"]))
        {
          $locale = $_SESSION["locale"];
          App::setLocale($locale);
          #echo("midl ".$locale."</br>");
        }
        else
        {
          $langs = array();
          if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
          {
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i',
              $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse
            );

            if (count($lang_parse[1]))
            {
              $langs = array_combine($lang_parse[1], $lang_parse[4]);
              foreach ($langs as $lang => $val)
              {
                if ($val === '')
                  $langs[$lang] = 1;
              }

              arsort($langs, SORT_NUMERIC);
            }
          }

          foreach ($langs as $lang => $val) { break; }

          if (stristr($lang,"-"))
          {
            $tmp = explode("-",$lang);
            $lang = $tmp[0]; 
          }
          $_SESSION["locale"] = $lang;
          App::setLocale($lang);
        }
        return $next($request);
    }
}
