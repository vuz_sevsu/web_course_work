<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_request extends Model
{
    protected $table = 'user_requests';

    public function getAllTable()
    {
      $res = User_request::orderBy('id','DESC')->paginate(3);
      return $res;
    }

    public function getRentRequestTable()
    {
      #var_dump(User_request::join('ships','user_requests.ship_id','=','ships.id')
      #->where('ships.type','rent')->select('user_requests.id', 'user_requests.first_name', 'user_requests.second_name', 'user_requests.number_phone', 'ships.main_title', 'ships.cover_photo')->toSql());
      $res = User_request::join('ships','user_requests.ship_id','=','ships.id')
      ->where('ships.type','rent')->where('user_requests.status','pending')->select('user_requests.id', 'user_requests.first_name', 'user_requests.second_name', 'user_requests.number_phone', 'ships.main_title', 'ships.cover_photo')->paginate(3);
      return $res;
    }

    public function getPurchasesRequestTable()
    {
      $res = User_request::join('ships','user_requests.ship_id','=','ships.id')
      ->where('ships.type','purchase')->where('user_requests.status','pending')->select('user_requests.id', 'user_requests.first_name', 'user_requests.second_name', 'user_requests.number_phone', 'ships.main_title', 'ships.cover_photo')->paginate(3);
      return $res;
    }

    public function getAdminPendingRequests($id)
    {
      #var_dump(User_request::join('ships','user_requests.ship_id','=','ships.id')->where('user_requests.id','=',$id)->get()->toArray());
      $res = User_request::join('ships','user_requests.ship_id','=','ships.id')
      ->where('user_requests.id',$id)->get()->toArray();
      $res['requests_id_value'] = $id;
      return $res;
    }

    public function getInWorkRequests()
    {
      $res = User_request::join('ships','user_requests.ship_id','=','ships.id')
      ->where('user_requests.status','in_work')->select('user_requests.id', 'user_requests.first_name', 'user_requests.second_name', 'user_requests.number_phone', 'ships.main_title', 'ships.cover_photo')->paginate(3);
      return $res;
    }


}
