<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $table = 'ships';

    public function getAllTable()
    {
      $res = Ship::orderBy('id','DESC')->paginate(3);
      return $res;
    }

    public function getShip($id)
    {
      $res = Ship::where('id',$id)->get()->toArray();
      return $res;
    }

    public function getRentShipsForUser()
    {
      $res = Ship::where('type','rent')->where('status','free')->orderBy('id','DESC')->paginate(3);
      return $res;
    }

    public function getPurchaseShipsForUser()
    {
      $res = Ship::where('type','purchase')->where('status','free')->orderBy('id','DESC')->paginate(3);
      return $res;
    }
}
