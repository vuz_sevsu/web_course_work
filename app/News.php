<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public function getAllTable()
    {
      $res = News::orderBy('id','DESC')->paginate(3);
      return $res;
    }

    public function getNews($id)
    {
      $res = News::where('id',$id)->get()->toArray();
      return $res;
    }
}
