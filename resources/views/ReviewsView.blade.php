<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Reviews";
  $str = file_get_contents("assets/pages_storage/reviews_page.json");
  $info_json = json_decode($str, true);
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('reviews_page.main_title')</h1>
  </div>
  <div class="part">
    <?php
      if (!isset($_SESSION["isAdmin"]))
        echo($info_json[$_SESSION["locale"]]["brief_information"]);
      else
      {
    ?>
     <form method="post" action="{{ route('edit_reviews_information') }}">
       {{ csrf_field() }}
       <textarea id="mytextarea" name="mytextarea" >
         <?php echo($info_json[$_SESSION["locale"]]["brief_information"]); ?>
       </textarea>
       <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
     </form>
   <?php }?>
  </div>
  <div class="part">
    <div id="mc-review"></div>
    <script type="text/javascript">
    cackle_widget = window.cackle_widget || [];
    cackle_widget.push({widget: 'Review', id: 75217});
    (function() {
        var mc = document.createElement('script');
        mc.type = 'text/javascript';
        mc.async = true;
        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
    })();
    </script>
    <a id="mc-link" href="http://cackle.me">Отзывы для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>
  </div>
</section>
@endsection
