<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Buying a vessel";
  $str = file_get_contents("assets/pages_storage/purchases_page.json");
  $info_json = json_decode($str, true);
  $content = $model->getPurchaseShipsForUser();
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('purchases_page.main_title')</h1>
  </div>
  @if ((isset($_SESSION["isAdmin"])))
    <div class="part">
      <div class="text-right"><a class="btn btn-primary" href="{{ route('add_purchase') }}">Добавить судно</a></div>
    </div>
  @endif
  <div class="part">
    @if (!isset($_SESSION["isAdmin"]))
        <?php echo $info_json[$_SESSION["locale"]]["brief_information"]; ?>
    @else
       <form method="post" action="{{ route('edit_rental_page_information') }}">
         {{ csrf_field() }}
         <textarea id="mytextarea" name="mytextarea" >
           <?php echo($info_json[$_SESSION["locale"]]["brief_information"]); ?>
         </textarea>
         <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
       </form>
    @endif
  </div>

  <div class="part">
    @foreach ($content as $value)
      <div class="news_part">
        <div class="news-cover_photo">
          <img src ="{{$value['cover_photo']}}">
        </div>
        <div class="news_text_part">
          <h3>{{$value['main_title']}}</h3>
          <p>{{$value['short_text']}}</p>
          <a href = "open_ship/{{$value['id']}}" class = "news_more_link">@lang('buttons.read_more')</a>
        </div>
      </div>
    @endforeach
    {{ $content->links() }}
  </div>
</section>
@endsection
