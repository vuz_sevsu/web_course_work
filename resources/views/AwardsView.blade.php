<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Awards";
  $str = file_get_contents("assets/pages_storage/awards_page.json");
  $info_json = json_decode($str, true);
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('awards_page.main_title')</h1>
  </div>
  <div class="part">
    <?php
      if (!isset($_SESSION["isAdmin"]))
        echo($info_json[$_SESSION["locale"]]["brief_information"]);
      else
      {
    ?>

     <form method="post" action="{{ route('edit_awards_information') }}">
       {{ csrf_field() }}
       <textarea id="mytextarea" name="mytextarea" >
         <?php echo($info_json[$_SESSION["locale"]]["brief_information"]); ?>
       </textarea>
       <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
     </form>
     <?php
       }?>
  </div>
</section>
@endsection
