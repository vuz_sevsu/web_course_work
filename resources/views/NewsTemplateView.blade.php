<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "News";
  $str = file_get_contents("assets/pages_storage/news_page.json");
  $info_json = json_decode($str, true);
  $add_flag = false;
  $id_flag = false;
  if (isset($news_info))
  {
    $id_flag = true;
    #print_r($news_info);
  }
  else
  {
    $add_flag = true;
  }

?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('news_page.main_title')</h1>
  </div>
  @if (isset($_SESSION["isAdmin"]))
    @if ($add_flag)
      <div class="part">
        <form method="post" action="{{ route('save_news') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <p>
            Язык для новости</br> <select name="language">
              <option selected value = "ru">Русский</option>
              <option value = "en">English</option>
            </select>
          </p>
          <p>Изображение обложки</br> <input type="file" name="cover_photo" required></p>
          <p>Заглавие</br><textarea name="main_title" required></textarea></p>
          <p>Краткий текст</br><textarea name="short_text" required></textarea></p>
          <p>Основной контент</br><textarea id="mytextarea" name="main_content" ></textarea></p>
          <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
        </form>
      </div>
    @else
      <div class="part">
        <?php #print_r($info); //?>
        <form method="post" action="../edit_news/{{$news_info['0']['id']}}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <!--<p>
            Язык для новости</br> <select name="language">
              <option value = "ru">Русский</option>
              <option value = "en">English</option>
            </select>
          </p>-->
          <img class="cover_photo" src="{{$news_info['0']['cover_photo']}}">
          <p>Изображение обложки</br> <input type="file" name="cover_photo"></p>
          <p>Заглавие</br><textarea name="main_title" required>{{$news_info['0']['main_title']}}</textarea></p>
          <p>Краткий текст</br><textarea name="short_text" required>{{$news_info['0']['short_text']}}</textarea></p>
          <p>Основной контент</br><textarea id="mytextarea" name="main_content">{{$news_info['0']['main_content']}}</textarea></p>
          <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
        </form>
      </div>
    @endif
  @elseif ($id_flag)
    <div class="part">
      <img class="cover_photo" src="{{$news_info['0']['cover_photo']}}">
        <h2>{{$news_info['0']['main_title']}}</h2>
        <p><?php echo $news_info['0']['main_content']; ?></p>
      </form>
    </div>
  @endif
</section>
@endsection
