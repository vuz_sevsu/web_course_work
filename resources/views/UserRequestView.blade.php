<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Request";
  #$str = file_get_contents("assets/pages_storage/news_page.json");
  #$info_json = json_decode($str, true);
  $add_flag = false;
  $id_flag = false;
  if (isset($request_info))
  {
    $id_flag = true;
    #print_r($news_info);
  }
  else
  {
    $add_flag = true;
  }

?>
@extends($template)


@section('content')
<section class="col content">
  @if(isset($_title))
    <div class="part">
      <h1> kkkkkk </h1>
    </div>
  @endif
  @if (isset($_SESSION["isUser"]))
    @if ($add_flag)
      <div class="part">
        <form method="post" action="../save_request/{{$ship_id}}">
          {{ csrf_field() }}
          <p>Номер телефона</br><input type="text" name="number_phone" required> м</p>
          <p>Почта</br><input type="text" name="mail" required> м</p>
          <p>Имя</br><input type="text" name="first_name" required> м</p>
          <p>Фамилия</br><input type="text" name="second_name"required> м</p>
          <p>Отчество</br><input type="text" name="patronymic" required> м</p>
          <p>Комментарий</br><textarea id="mytextarea" name="comment"></textarea></p>
          <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
        </form>
      </div>
    @elseif ($id_flag)
    <!--  <div class="part">

        <img class="cover_photo" src="">

      </div>-->
    @endif
  @elseif (isset($_SESSION["isAdmin"]) && $id_flag && isset($request_info[0]))
    <div class="part">
      <?php $value = $request_info[0]; ?>
      <h3>{{$value['main_title']}}</h3>
      <p>
        {{$value['first_name']}}</br>
        {{$value['second_name']}}</br>
        {{$value['number_phone']}}
      </p>
      <p>
        <span>Цена {{$value['price']}}</span></br>
        <span>Год {{$value['short_text']}}</span></br>
        <span>Длина, м {{$value['length_overall']}}</span></br>
        <span>Ширина, м {{$value['beam_overall']}}</span></br>
        <span>Водоизмещение, тонн {{$value['displacement']}}</span></br>
        <span>Максимальная осадка, м {{$value['draft']}}</span></br>
        <span>Количество кают {{$value['quantity_of_cabins']}}</span></br>
        <span>Спальных мест {{$value['sleeping_places']}}</span></br>
        <span>Дальность хода, миль {{$value['range']}}</span></br>
        <span>Запас топлива, л {{$value['fuel_capacity']}}</span></br>
        <span>Запас пресной воды {{$value['water_capacity']}}</span></br>
        <span>Круизная скорость, узлов {{$value['cruising_speed']}}</span></br>
      </p>

      <p><?php echo $value['main_content']; ?></p>
      <p><a href = "../open_ship/{{$value['ship_id']}}">к заявке</a></p>
      <form method="post" action="../edit_user_request/{{$request_info['requests_id_value']}}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success" name="in_work_button" value="in_work" >@lang('buttons.in_work')</button>
        <button type="submit" class="btn btn-success" name="done_button"value="done" >@lang('buttons.done')</button>
        <!--<button type="submit" class="btn btn-success" name="blocked_button" value="blocked">@lang('buttons.blocked')</button>-->
      </form>

    </div>
  @endif
</section>
@endsection
