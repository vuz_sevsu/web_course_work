<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "News";
  $str = file_get_contents("assets/pages_storage/news_page.json");
  $info_json = json_decode($str, true);
  $news_content = $model->getAllTable();
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('news_page.main_title')</h1>
  </div>
  <div class="part">
    <?php
      if (!isset($_SESSION["isAdmin"]))
        echo($info_json[$_SESSION["locale"]]["brief_information"]);
      else
      {
    ?>
     <form method="post" action="{{ route('edit_news_information') }}">
       {{ csrf_field() }}
       <textarea id="mytextarea" name="mytextarea" >
         <?php echo($info_json[$_SESSION["locale"]]["brief_information"]); ?>
       </textarea>
       <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
     </form>
   <?php }?>
  </div>
  @if (isset($_SESSION["isAdmin"]))
    <div class="part text-right">
      <a class="btn btn-primary" href="{{ route('add_news') }}">Создать новость</a>
    </div>
  @endif
  <div class="part">
    @foreach ($news_content as $value)
      <div class="news_part">
        <div class="news-cover_photo">
          <img src ="{{$value['cover_photo']}}">
        </div>
        <div class="news_text_part">
          <h3>{{$value['main_title']}}</h3>
          <p>{{$value['short_text']}}</p>
          <a href = "open_news/{{$value['id']}}" class = "news_more_link">@lang('buttons.read_more')</a>
        </div>
      </div>
    @endforeach
    {{ $news_content->links() }}
  </div>
</section>
@endsection
