<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Ship";
  #$str = file_get_contents("assets/pages_storage/news_page.json");
  #$info_json = json_decode($str, true);
  $add_flag = false;
  $id_flag = false;
  if (isset($ship_info))
  {
    $id_flag = true;
    #print_r($news_info);
  }
  else
  {
    $add_flag = true;
  }

?>
@extends($template)


@section('content')
<section class="col content">
  @if(isset($_title))
    <div class="part">
      <h1>  </h1>
    </div>
  @endif
  @if (isset($_SESSION["isAdmin"]))
    @if ($add_flag)
      <div class="part">
        <form method="post" action="{{ route('save_ship') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <p>Изображение обложки</br> <input type="file" name="cover_photo" required></p>
          <p>Название</br><textarea name="main_title" required></textarea></p>
          <!--<p>
            Язык для новости</br> <select name="language">
              <option selected value = "ru">Русский</option>
              <option value = "en">English</option>
            </select>
          </p>-->
          <p>Год</br><textarea name="short_text" required></textarea></p>
          <p>Основной контент</br><textarea id="mytextarea" name="main_content"></textarea></p>
          <p>Длина</br><input type="text" name="length_overall" required> м</p>
          <p>Ширина</br><input type="text" name="beam_overall" required> м</p>
          <p>Водоизмещение </br><input type="text" name="displacement" required> тонн</p>
          <p>Максимальная осадка </br><input type="text" name="draft" required> м</p>
          <p>Дальность хода </br><input type="text" name="range" required> миль</p>
          <p>Запас топлива </br><input type="text" name="fuel_capacity" required> л</p>
          <p>Запас пресной воды </br><input type="text" name="water_capacity" required> л</p>
          <p>Круизная скорость </br><input type="text" name="cruising_speed" required> узлов</p>
          <p>Количество кают</br><input type="text" name="quantity_of_cabins" required></p>
          <p>Спальных мест</br><input type="text" name="sleeping_places" required></p>
          <p>Цена</br><input type="text" name="price" required> руб</p>
          <p>Статус</br><select name="status">
            <option selected value = "free">Свободная</option>
            <option value = "consideration">На рассмотрение</option>
            <option value = "absent">Отсутствует</option>
            <option value = "sold_out">Продано</option>
          </select>
          </p>
          <p>Тип</br><select name="type">
            <option selected value = "rent">Аренда</option>
            <option value = "purchase">Покупка</option>
          </select>
          </p>
          <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
        </form>
      </div>
    @else
      <div class="part">
        <form method="post" action="../edit_ship/{{$ship_info['0']['id']}}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <img class="cover_photo" src="{{$ship_info['0']['cover_photo']}}">
          <p>Изображение обложки</br> <input type="file" name="cover_photo" ></p>
          <p>Название</br><textarea name="main_title" required>{{$ship_info['0']['main_title']}}</textarea></p>
          <!--<p>
            Язык для новости</br> <select name="language">
              <option selected value = "ru">Русский</option>
              <option value = "en">English</option>
            </select>
          </p>-->
          <p>Год производства</br><textarea name="short_text" required>{{$ship_info['0']['short_text']}}</textarea></p>
          <p>Основной контент</br><textarea id="mytextarea" name="main_content" >{{$ship_info['0']['main_content']}}</textarea></p>
          <p>Длина</br><input type="text" name="length_overall" value = "{{$ship_info['0']['length_overall']}}" required> м</p>
          <p>Ширина</br><input type="text" name="beam_overall" value = "{{$ship_info['0']['beam_overall']}}" required> м</p>
          <p>Водоизмещение </br><input type="text" name="displacement" value = "{{$ship_info['0']['displacement']}}" required> тонн</p>
          <p>Максимальная осадка </br><input type="text" name="draft" value = "{{$ship_info['0']['draft']}}" required> м</p>
          <p>Дальность хода </br><input type="text" name="range" value = "{{$ship_info['0']['range']}}" required> миль</p>
          <p>Запас топлива </br><input type="text" name="fuel_capacity" value = "{{$ship_info['0']['fuel_capacity']}}" required> л</p>
          <p>Запас пресной воды </br><input type="text" name="water_capacity" value = "{{$ship_info['0']['water_capacity']}}" required> л</p>
          <p>Круизная скорость </br><input type="text" name="cruising_speed" value = "{{$ship_info['0']['cruising_speed']}}" required> узлов</p>
          <p>Количество кают</br><input type="text" name="quantity_of_cabins" value = "{{$ship_info['0']['quantity_of_cabins']}}" required></p>
          <p>Спальных мест</br><input type="text" name="sleeping_places" value = "{{$ship_info['0']['sleeping_places']}}" required></p>
          <p>Цена</br><input type="text" name="price" value = "{{$ship_info['0']['price']}}" required> руб</p>
          <p>Статус</br><select name="status">
            <option <?php echo ($ship_info['0']['status']=="free")?"selected":"";?> value = "free">Свободная</option>
            <option <?php echo ($ship_info['0']['status']=="consideration")?"selected":"";?> value = "consideration">На рассмотрение</option>
            <option <?php echo ($ship_info['0']['status']=="absent")?"selected":"";?> value = "absent">Отсутствует</option>
            <option <?php echo ($ship_info['0']['status']=="sold_out")?"selected":"";?> value = "sold_out">Продано</option>
          </select>
          </p>
          <p>Тип</br><select name="type">
            <option <?php echo ($ship_info['0']['type']=="rent")?"selected":"";?> value = "rent">Аренда</option>
            <option <?php echo ($ship_info['0']['type']=="purchase")?"selected":"";?> value = "purchase">Покупка</option>
          </select>
          </p>
          <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
        </form>
      </div>
    @endif
  @elseif ($id_flag)
    <div class="part">
      <img class="cover_photo" src="{{$ship_info['0']['cover_photo']}}">
      @if ((isset($_SESSION["isUser"])))
          <a class="btn btn-primary pull-right" href="../add_request/{{$ship_info['0']['id']}}">Подать заявку</a>
      @endif
      <h2>{{$ship_info['0']['main_title']}}</h2>
      <p>
        <span>Цена {{$ship_info['0']['price']}}</span></br>
        <span>Год {{$ship_info['0']['short_text']}}</span></br>
        <span>Длина, м {{$ship_info['0']['length_overall']}}</span></br>
        <span>Ширина, м {{$ship_info['0']['beam_overall']}}</span></br>
        <span>Водоизмещение, тонн {{$ship_info['0']['displacement']}}</span></br>
        <span>Максимальная осадка, м {{$ship_info['0']['draft']}}</span></br>
        <span>Количество кают {{$ship_info['0']['quantity_of_cabins']}}</span></br>
        <span>Спальных мест {{$ship_info['0']['sleeping_places']}}</span></br>
        <span>Дальность хода, миль {{$ship_info['0']['range']}}</span></br>
        <span>Запас топлива, л {{$ship_info['0']['fuel_capacity']}}</span></br>
        <span>Запас пресной воды {{$ship_info['0']['water_capacity']}}</span></br>
        <span>Круизная скорость, узлов {{$ship_info['0']['cruising_speed']}}</span></br>
      </p>
      <p><?php echo $ship_info['0']['main_content']; ?></p>

    </div>
  @endif
</section>
@endsection
