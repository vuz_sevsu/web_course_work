<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Rent";
  $str = file_get_contents("assets/pages_storage/news_page.json");
  $info_json = json_decode($str, true);
  $requests = $model->getRentRequestTable();
  //$ship_info = $ship_model->getShip($value['ship_id']);
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>Заявки аренды в статусе ожидания</h1>
  </div>

  <div class="part">
    @foreach ($requests as $value)
      <div class="news_part">
        <div class="news-cover_photo">
          <img src ="{{$value['cover_photo']}}">
        </div>
        <div class='news_text_part'>
          <h3>{{$value['main_title']}}</h3>
          <p>
            {{$value['first_name']}}</br>
            {{$value['second_name']}}</br>
            {{$value['number_phone']}}
          </p>
          <a href = "admin_pending/{{$value['id']}}" class = "news_more_link">@lang('buttons.read_more')</a>
        </div>
      </div>
    @endforeach
    {{ $requests->links() }}
  </div>
</section>
@endsection
