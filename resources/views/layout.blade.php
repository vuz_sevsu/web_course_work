<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <title>{{$title}}</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/style.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/menu.css') }}"/>


    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <script
      src="https://code.jquery.com/jquery-3.4.1.js"
      integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
      crossorigin="anonymous">
    </script>
    <script src="https://cdn.tiny.cloud/1/3zqkga6cizk2buca99jvhv91wox825muydka4vrpndrxgu7g/tinymce/5/tinymce.min.js"
    referrerpolicy="origin">
    </script>
    <script>
      tinymce.init({
        selector: '#mytextarea'
      });
    </script>
    <script src="{{ asset('assets/js/menu.js') }}"></script>

  </head>

  <body>

  <div class="wrapper container">
    <div class="row">
      <header><img src="{{ asset('assets/images/header01.png')}}"></header>
    </div>
    <div class="row">
      <!--menu-->
      <menu class="col-md-3">
        <ul class="accordion">
          <li>
            <a href = "{{ route('main') }}"><div class="link"><i class="fa fa-globe"></i>@lang('layout.menu_main_button')</div></a>
          </li>
          <li>
            <a href = "{{ route('news') }}"><div class="link"><i class="fa fa-newspaper-o"></i>@lang('layout.menu_news_button')</div></a>
          </li>
          <li>
            <div class="link"><i class="fa fa-book"></i>@lang('layout.menu_about_button')<i class="fa fa-chevron-down"></i></div>
            <ul class="submenu">
              <li><a href="{{ route('history') }}">@lang('layout.menu_history_button')</a></li>
              <li><a href="{{ route('services') }}">@lang('layout.menu_services_button')</a></li>
              <li><a href="{{ route('awards') }}">@lang('layout.menu_awards_button')</a></li>
              <li><a href="{{ route('reviews') }}">@lang('layout.menu_reviews_button')</a></li>
            </ul>
          </li>
          <li>
            <a href = "{{ route('boat_rental') }}"><div class="link"><i class="fa fa-credit-card" aria-hidden="true"></i>@lang('layout.menu_rent_button')</div></a>
          </li>
          <li>
            <a href = "{{ route('buying_a_vessel') }}"><div class="link"><i class="fa fa-credit-card" aria-hidden="true"></i>@lang('layout.menu_purchase_button')</div></a>
          </li>
          <li>
            <a href = "{{ route('contacts') }}"><div class="link"><i class="fa fa-map-o"></i>@lang('layout.menu_contacts_button')</div></a>
          </li>
        </ul>


        <ul class="accordion">

          <li>
            <div class="link"><i class="fa fa-language" aria-hidden="true"></i>
              <?php
                  if (!isset($_SESSION)) session_start();
                  $current_lang_image = "";
                  $current_lang_text = "";
                  if (isset($_SESSION["locale"]))
                  {
                    switch($_SESSION["locale"])
                    {
                      case "ru":
                        ?><img src="{{ asset('assets/images/language/ru.gif') }}" alt="russion language" title="russion"> Русский<?php
                        break;
                      case "en":
                        ?><img src="{{ asset('assets/images/language/en_gb.gif') }}" alt="english language" title="english"> English<?php
                        break;
                    }
                  }
              ?>
            <i class="fa fa-chevron-down"></i></div>
            <ul class="submenu">
              <li><a href="{{route('locale','ru')}}"><img src="{{ asset('assets/images/language/ru.gif') }}" alt="russion language" title="russion"> Русский</a></li>
              <li><a href="{{route('locale','en')}}"><img src="{{ asset('assets/images/language/en_gb.gif') }}" alt="english language" title="english"> English</a></li>
            </ul>
          </li>

          <li>
            <?php if (!isset($_SESSION)) session_start(); ?>
            @if (isset($_SESSION['isUser']))
              <a href = "{{route('end_session')}}"><div class="link"><i class="fa fa-sign-out" aria-hidden="true"></i>@lang('layout.menu_sign_out_button')</div></a>
            @else
              <a href = "{{route('login')}}"><div class="link"><i class="fa fa-sign-in" aria-hidden="true"></i>@lang('layout.menu_sign_in_button')</div></a>
            @endif
          </li>
        </ul>

      </menu>
      <!--content-->
      @yield('content')
      <?php# include 'app/views/'.$content_view ?>
    </div>
    <div class="row">
      <footer><img src="{{ asset('assets/images/footer01.png')}}">dfgwh 2020 (sevsu pi-17)</footer>
    </div>

  </div>


</body>
