<!DOCTYPE html>
<?php
  if (!isset($_SESSION))
    session_start();
  $template = (isset($_SESSION["isAdmin"])) ? 'admin_layout' : 'layout';
  $title = "Contacts";
  $str = file_get_contents("assets/pages_storage/contacts_page.json");
  $info_json = json_decode($str, true);
?>
@extends($template)
<!--{{$template}}-->

@section('content')
<section class="col content">
  <div class="part">
    <h1>@lang('contacts_page.main_title')</h1>
  </div>
  <div class="part">
    <?php
      if (!isset($_SESSION["isAdmin"]))
        echo($info_json[$_SESSION["locale"]]["brief_information"]);
      else
      {
    ?>

     <form method="post" action="{{ route('edit_contacts_information') }}">
       {{ csrf_field() }}
       <textarea id="mytextarea" name="mytextarea" >
         <?php echo($info_json[$_SESSION["locale"]]["brief_information"]); ?>
       </textarea>
       <button type="submit" class="btn btn-success">@lang('buttons.save')</button>
     </form>
   <?php }?>
  </div>
  <div class= "part">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aeeeb303d08ba197a670a91015660a1e61083e4a4f673e70acf3cde0e13bdfe78&amp;width=823&amp;height=538&amp;lang=ru_RU&amp;scroll=true"></script>
  </div>
</section>
@endsection
