<?php

return [
  'save'=>'Save',
  'edit'=>'Edit',
  'clear'=>'Clear',
  'read_more'=>'Read more',
  'in_work'=>'In work',
  'done'=>'Done',
  'blocked'=>'Blocked',
];

?>
