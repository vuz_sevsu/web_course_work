<?php

return [
  'menu_main_button' => 'Main page',
  'menu_news_button' => 'News',
  'menu_about_button' => 'About company',
    'menu_history_button' => 'History',
    'menu_services_button' => 'Services',
    'menu_awards_button' => 'Awards',
    'menu_reviews_button' => 'Reviews',
  'menu_rent_button' => 'Boat rental',
  'menu_purchase_button' => 'Buying a vessel',
  'menu_contacts_button' => 'Contacts',
  'menu_sign_in_button' => 'Sign in',
  'menu_sign_out_button' => 'Sign out',
];

?>
