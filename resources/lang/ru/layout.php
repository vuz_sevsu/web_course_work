<?php

return [
  'menu_main_button'=>'Главная страница',
  'menu_news_button'=>'Новости',
  'menu_about_button'=>'О компании',
    'menu_history_button' => 'История',
    'menu_services_button' => 'Услуги',
    'menu_awards_button' => 'Награды',
    'menu_reviews_button' => 'Отзывы',
  'menu_rent_button' => 'Аренда судна',
  'menu_purchase_button' => 'Покупка судна',
  'menu_contacts_button' => 'Контакты',
  'menu_sign_in_button' => 'Вход',
  'menu_sign_out_button' => 'Выход',
];

?>
