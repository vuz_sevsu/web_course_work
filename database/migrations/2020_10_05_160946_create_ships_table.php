<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->id();
            $table->longText('cover_photo');
            $table->longText('main_title');
            $table->longText('short_text');//год
            $table->longText('main_content');
            $table->string('length_overall');
            $table->string('beam_overall');
            $table->string('displacement');
            $table->string('draft');
            $table->string('quantity_of_cabins');
            $table->string('range');
            $table->string('fuel_capacity');
            $table->string('water_capacity');
            $table->string('cruising_speed');
            $table->string('sleeping_places');
            $table->string('price');
            $table->string('status');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ships');
    }
}
