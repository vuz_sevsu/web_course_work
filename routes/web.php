<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storages;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainPageController@index')->name('main');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/history', 'HistoryController@index')->name('history');
Route::get('/services', 'ServicesController@index')->name('services');
Route::get('/awards', 'AwardsController@index')->name('awards');
Route::get('/reviews', 'ReviewsController@index')->name('reviews');
Route::get('/boat_rental', 'BoatRentalController@index')->name('boat_rental');
Route::get('/buying_a_vessel', 'BuyingAVesselController@index')->name('buying_a_vessel');
Route::get('/contacts', 'ContactsController@index')->name('contacts');

Route::get('/admin_rent_pending', 'UserReqestController@admin_rent_pending')->name('admin_rent_pending');
Route::get('/admin_purchase_pending', 'UserReqestController@admin_purchase_pending')->name('admin_purchase_pending');
Route::get('/admin_requests_in_work', 'UserReqestController@admin_requests_in_work')->name('admin_requests_in_work');


Route::get('locale/{locale}','LanguageController@changeLocale')->name('locale');

Route::get('/after_auth','AuthController@after_auth')->name('after_auth');
Route::get('/end_session','AuthController@end_session')->name('end_session');

Route::post('/edit_brief_information', 'MainPageController@edit_brief_information')->name('edit_brief_information');
Route::post('/edit_news_information', 'NewsController@edit_information')->name('edit_news_information');
Route::post('/edit_history_information', 'HistoryController@edit_information')->name('edit_history_information');
Route::post('/edit_services_information', 'ServicesController@edit_information')->name('edit_services_information');
Route::post('/edit_awards_information', 'AwardsController@edit_information')->name('edit_awards_information');
Route::post('/edit_reviews_information', 'ReviewsController@edit_information')->name('edit_reviews_information');
Route::post('/edit_contacts_information', 'ContactsController@edit_information')->name('edit_contacts_information');
Route::post('/edit_purchases_page_information', 'BuyingAVesselController@edit_information')->name('edit_purchases_page_information');
Route::post('/edit_rental_page_information', 'BoatRentalController@edit_information')->name('edit_rental_page_information');

Route::get('/add_news', 'NewsController@add_news')->name('add_news');
Route::post('/save_news', 'NewsController@save_news')->name('save_news');
Route::get('open_news/{news_id}', 'NewsController@open_news_for_id')->name('open_news');
Route::post('edit_news/{news_id}', 'NewsController@edit_news_for_id')->name('edit_news');

Route::get('/add_rent', 'BoatRentalController@add_rent')->name('add_rent');
Route::get('/add_purchase', 'BuyingAVesselController@add_purchase')->name('add_purchase');
Route::post('/save_ship', 'MainShipController@save_ship')->name('save_ship');
Route::get('open_ship/{id}', 'MainShipController@open_ship_for_id')->name('open_ship');
Route::post('edit_ship/{id}', 'MainShipController@edit_ship_for_id')->name('edit_ship');

Route::get('add_request/{ship_id}', 'UserReqestController@add_request')->name('add_request');
Route::post('save_request/{id}', 'UserReqestController@save_request')->name('save_request');
Route::post('edit_user_request/{id}', 'UserReqestController@edit_user_request')->name('edit_user_request');
Route::get('admin_pending/{id}', 'UserReqestController@admin_pending')->name('admin_pending');
